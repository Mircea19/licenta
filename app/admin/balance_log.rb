ActiveAdmin.register BalanceLog do

  actions :index
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  index do
    column :id
    column :client_id do |log|
      link_to log.client.name, admin_client_path(log.client)
    end
    column :coach_id do |log|
      if log.coach.present?
        link_to log.coach.name, admin_client_path(log.coach)
      else
        "N/A"
      end
    end
    column :from
    column :amount
  end




end
