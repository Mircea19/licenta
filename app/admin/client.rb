ActiveAdmin.register Client do

  actions :all
  action_item only: :index do
    link_to 'Add Tokens', tokens_admin_clients_path
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  collection_action :tokens, method: :get
  collection_action :tokens, method: :post do
    redirect_to :back
  end

  controller do

    def tokens
      if params[:client].present?
        client = Client.find(params[:client][:id])
        tokens = params[:tokens].to_i + client.tokens
        if params[:tokens].to_i < 0
          flash[:notice] = "Tokens must be greater than 0."
          redirect_to :back
        else
          client.update(tokens: tokens)
          BalanceLog.create(amount: tokens, client_id: client.id, from: "admin")
          # BalanceLog.create(transfered_to: user.id,
          #                   tokens: params[:tokens].to_i,
          #                   current_amount: user.tokens,
          #                   previous_amount: user.tokens - params[:tokens].to_i,
          #                   received_from: "HeyCoach",
          #                   transfered_to_name: "#{name}")

          flash[:notice] = "Tokens were added"
          redirect_to :back
        end
      end
    end
  end

end
