ActiveAdmin.register User do

  show do
    attributes_table do
      row :id
      row :email
      row :current_sign_in_at
      row :sign_in_count
      row :user_type
    end
  end

  index do
    column :id
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :account_type
    actions
    column "Sign In As" do |user|
      token = Devise.friendly_token
      user.sign_in_token = token
      user.save
      link_to "Sign in as", sign_in_from_admin_path(id: user.id, token: token), method: :post
    end
  end

  filter :email
  filter :sign_in_count
  filter :last_sign_at

  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :account_type, input_html: { disabled: true } 
    end
    f.actions
  end



end
