module Api
class TwilioController < BaseController

  def confirm_coach
    twiml = Twilio::TwiML::Response.new do |r|
      r.Gather numDigits: '1', timeout: '15', action: "/api/twilio/after_confirmation_coach?coaching_date_id=#{params["coaching_date_id"]}", method: 'get' do |g|
        g.Say 'Hello and welcome to Coaching App. Please press 1 to begin'
      end
    end
    render xml: twiml.to_xml
  end

  def confirm_user
    twiml = Twilio::TwiML::Response.new do |r|
      r.Gather numDigits: '1', timeout: '15', action: "/api/twilio/after_confirmation_user?coaching_date_id=#{params["coaching_date_id"]}", method: 'get' do |g|
        g.Say 'Hello and welcome to Coaching App. Please press 1 to begin'
      end
    end
    render xml: twiml.to_xml
  end

  def after_confirmation_coach
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @call = @client.account.calls.get(params["CallSid"])
    coaching_date = CoachingDate.find(params["coaching_date_id"])
    if coaching_date.blank?
      puts "Error in after confirmation coach at #{DateTime.now}, call_sid: #{@call.sid}"
    end
    if params['Digits'] == '1'
      call_log = CallLog.new
      call_log.coaching_date_id = coaching_date.id
      call_log.status = "coach_joined"
      call_log.ended_because = "coach"
      call_log.save
      @call.update(url: self.twilio_url + "/api/twilio/hold_coach",
          method: "GET")
    else
      @call.update(status: "completed")
    end
    render nothing: true
  end


  def after_confirmation_user
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @call = @client.account.calls.get(params["CallSid"])
    coaching_date = CoachingDate.find(params["coaching_date_id"])
    coach_number = @call.to
    if coaching_date.blank?
      puts "Error in after confirmation user at #{DateTime.now}, call_sid: #{@call.sid}"
    end
    if params['Digits'] == '1'
      call_log = CallLog.new
      call_log.coaching_date_id = coaching_date.id
      call_log.status = "user_joined"
      call_log.ended_because = "user"
      call_log.save
      @call.update(url: self.twilio_url + "/api/twilio/hold_user",
          method: "GET")
    else
      @call.update(status: "completed")
    end
    render nothing: true
  end


  def hold_coach
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'Please wait until we connect you to the user'
      r.Play 'http://coaching-app.s3.amazonaws.com/you_got_a_friend_in_me.mp3'
    end
    render xml: twiml.to_xml
  end

  def hold_user
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'Please wait until we connect you to the coach'
      r.Play 'http://coaching-app.s3.amazonaws.com/you_got_a_friend_in_me.mp3'
    end
    render xml: twiml.to_xml
  end

  def join_coach
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'You are about to be connected to the user'
      r.Dial do |j|
        j.Conference "MyConference#{params[:coach_number]}fc", endConferenceOnExit: false
      end
    end
    render xml: twiml.to_xml
  end

  def join_user
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'You are about to be connected to the coach'
      r.Dial do |j|
        j.Conference "MyConference#{params[:coach_number]}fc", endConferenceOnExit: false
      end
    end
    render xml: twiml.to_xml
  end

  def rejoin_user
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'Please wait while we reconnect you to the coach'
      r.Play 'http://coaching-app.s3.amazonaws.com/you_got_a_friend_in_me.mp3'
    end
    render xml: twiml.to_xml
  end

  def rejoin_coach
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'Please wait while we reconnect you to the user'
      r.Play 'http://coaching-app.s3.amazonaws.com/you_got_a_friend_in_me.mp3'
    end
    render xml: twiml.to_xml
  end

  def hold_user_after_joined
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'The coach has hung up. Please wait while we reconnect you.'
      r.Play 'http://coaching-app.s3.amazonaws.com/you_got_a_friend_in_me.mp3'
    end
    render xml: twiml.to_xml
  end

  def hold_coach_after_joined
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'The user hung up. Please wait while we reconnect you.'
      r.Play 'http://coaching-app.s3.amazonaws.com/you_got_a_friend_in_me.mp3'
    end
    render xml: twiml.to_xml
  end

  def end_user
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'We are sorry, but the coach is not available. The call will now end. Goodbye!'
    end
    render xml: twiml.to_xml
  end

  def end_coach
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'We are sorry, but the user is not available. The call will now end. Goodbye!'
    end
    render xml: twiml.to_xml
  end

  def finished_session_coach
    twiml = Twilio::TwiML::Response.new do |r|
      r.Say 'Thank you for using Coaching App. The session is now over. The call will end. Goodbye!'
    end
    render xml: twiml.to_xml
  end

  def submit_review
    twiml = Twilio::TwiML::Response.new do |r|
      r.Gather numDigits: '1', timeout: '10', action: '/api/twilio/after_first_review_enter', method: 'get' do |g|
        g.Say 'The session has ended.'
        g.Say 'Please review the coach using a grade from 1 to 5.'
      end
    end
    render xml: twiml.to_xml
  end

  def after_first_review_enter
    coaching_date = CoachingDate.find_by(client_call_sid: params["CallSid"])
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @call = @client.account.calls.get(params["CallSid"])
    grade = params['Digits'].to_i
    if grade >=1 && grade <=5
      Review.create(coach_id: coaching_date.coach.id, client_id: coaching_date.client.id, coaching_date_id: coaching_date.id, coach_grade: grade)
      @call.update(url: self.twilio_url + "/api/twilio/submit_second_grade",
          method: "GET")
    else
      @call.update(url: self.twilio_url + "/api/twilio/invalid_first_grade",
          method: "GET")
    end
    render nothing: true
  end

  def invalid_first_grade
    twiml = Twilio::TwiML::Response.new do |r|
      r.Gather numDigits: '1', timeout: '10', action: '/api/twilio/after_first_review_enter', method: 'get' do |g|
        g.Say 'Invalid grade.'
        g.Say 'Please review the coach using a grade from 1 to 5.'
      end
    end
    render xml: twiml.to_xml
  end

  def submit_second_grade
    twiml = Twilio::TwiML::Response.new do |r|
      r.Gather numDigits: '1', timeout: '10', action: '/api/twilio/after_second_review_enter', method: 'get' do |g|
        g.Say 'Please review the session using a grade from 1 to 5.'
      end
    end
    render xml: twiml.to_xml
  end

  def after_second_review_enter
    coaching_date = CoachingDate.find_by(client_call_sid: params["CallSid"])
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @call = @client.account.calls.get(params["CallSid"])
    grade = params['Digits'].to_i
    if grade >=1 && grade <=5
      review = coaching_date.review
      review.session_grade = grade
      review.save
      @call.update(url: self.twilio_url + "/api/twilio/finished_session_coach",
          method: "GET")
    else
      @call.update(url: self.twilio_url + "/api/twilio/invalid_second_grade",
          method: "GET")
    end
    render nothing: true
  end

  def invalid_second_grade
    twiml = Twilio::TwiML::Response.new do |r|
      r.Gather numDigits: '1', timeout: '10', action: '/api/twilio/after_second_review_enter', method: 'get' do |g|
        g.Say 'Invalid grade.'
        g.Say 'Please review the session using a grade from 1 to 5.'
      end
    end
    render xml: twiml.to_xml
  end
  

  def call_ended_user
    coaching_dates = CoachingDate.where(client_call_sid: params["CallSid"])
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @call = @client.account.calls.get(params["CallSid"])
    hash = {"busy" => "busy", "failed" => "failed", "no-answer" => "no_answer", "completed" => "hung_up"}
    coaching_dates.each do |coaching_date|
      if hash.has_key?(@call.status)
        call_log = CallLog.new
        call_log.coaching_date_id = coaching_date.id
        call_log.ended_because = "user"
        call_log.status = hash[@call.status]
        call_log.save
      end
    end
    render nothing: true
  end

  def call_ended_coach
    coaching_dates = CoachingDate.where(coach_call_sid: params["CallSid"])
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @call = @client.account.calls.get(params["CallSid"])
    hash = {"busy" => "busy", "failed" => "failed", "no-answer" => "no_answer", "completed" => "hung_up"}
    coaching_dates.each do |coaching_date|
      if hash.has_key?(@call.status)
        call_log = CallLog.new
        call_log.coaching_date_id = coaching_date.id
        call_log.ended_because = "coach"
        call_log.status = hash[@call.status]
        call_log.save
      end
    end
    render nothing: true
  end

  def twilio_url
    #return "https://app.geniuz.com"
    return "http://2b31832b.ngrok.io"
  end

end 
end
