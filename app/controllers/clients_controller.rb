class ClientsController < ApplicationController

  def edit 
    @client = Client.find(params[:id])
  end

  def update
    @client = Client.find(params[:id])
    success = true
    if @client.phone != client_params[:phone]
      token = 4.times.map{rand(10)}.join
      success = SendTwilioMessage.send_message(client_params[:phone], token)
      @client.validated_phone = false
      @client.phone_code = token
      @client.save
    end
    if @client.update(client_params)
      if success 
        flash[:notice] = "Profile successfully updated!"
      else
        flash[:notice] = "Invalid phone number. Please provide a valid one!"
      end
    else
      flash[:notice] = "Error while updating profile. Please try again!"
    end
    redirect_to :back
  end

  def verify_phone
    @client = Client.find(params[:id])
  end

  def resend_code
    @client = Client.find(params[:id])
    token = 4.times.map{rand(10)}.join
    @client.phone_code = token
    @client.save
    success = SendTwilioMessage.send_message(@client.phone, token)
    if success
      flash[:notice] = "Code successfully resent!"
      redirect_to :back
    else
      flash[:notice] = "Invalid phone number. Please provide a valid one!"
      redirect_to edit_client_path(@client)
    end
  end

  def verify_token
    token = params[:token]
    client = Client.find(params[:id])
    if token == client.phone_code
      client.validated_phone = true
      client.save
      flash[:notice] = "Phone successfully verified!"
      redirect_to edit_client_path(client)
    else
      flash[:notice] = "Incorrect code. Please try again!"
      redirect_to :back
    end
  end

  private 
  def client_params
    params.require(:client).permit(:first_name, :last_name, :about, :occupation, :phone)
  end
end
