class CoachesController < ApplicationController
  def edit
    @coach = Coach.find(params[:id])
  end

  def update
    @coach = Coach.find(params[:id])
    success = true
    if @coach.phone != coach_params[:phone]
      token = 4.times.map{rand(10)}.join
      success = SendTwilioMessage.send_message(coach_params[:phone], token)
      @coach.validated_phone = false
      @coach.phone_code = token
      @coach.save
    end
    if @coach.update(coach_params)
      if success 
        flash[:notice] = "Profile successfully updated!"
      else
        flash[:notice] = "Invalid phone number. Please provide a valid one!"
      end
    elsif !success
      flash[:notice] = "Invalid phone number. Please provide a valid one!"
    else
      flash[:notice] = "Error while updating profile. Please try again!"
    end
    redirect_to :back
  end

  def show
    @coach = Coach.find(params[:id])
    @coaching_dates = @coach.coaching_dates.upcoming.available
    @average = @coach.reviews.map{|r| r.coach_grade}.inject{ |sum, el| sum + el }.to_f / @coach.reviews.count
    @average_rating = @coach.reviews.present? ? "#{@average.round(2)}/5" : "N/A"
  end

  def set_coaching_dates
    @coach = Coach.find(params[:id])
    authorize! :coaching_dates, @coach
    @coaching_dates = @coach.coaching_dates.upcoming
  end

  def financial_details
    @coach = Coach.find(params[:id])
  end

  def set_financial_details
    @coach = Coach.find(params[:id])
    authorize! :coaching_dates, @coach
    @coach.rate = params[:rate]
    if @coach.save
      flash[:notice] = "Rate successfully updated!"
      redirect_to set_coaching_dates_coach_path(@coach)
    else
      flash[:motice] = "Error while setting rate. Please try again!"
      redirect_to :back
    end
  end

  def add_coaching_dates
    @coach = Coach.find(params[:id])
    starting = DateTime.strptime(params[:date] + " " + params[:starting_hour].to_s + ":" +params[:starting_minutes].to_s + " "+ params[:starting_am], "%m/%d/%Y %l:%M %p")
    ending = DateTime.strptime(params[:date] + " " + params[:ending_hour].to_s + ":" +params[:ending_minutes].to_s + " "+ params[:ending_am], "%m/%d/%Y %l:%M %p")
    if starting < ending
      while starting < ending
        if @coach.coaching_dates.where(starting: starting).limit(1).blank?
          c = CoachingDate.new
          c.starting = starting
          c.coach = @coach
          c.duration = 30
          c.paid_duration = 20  
          c.save
        end
        starting += 30.minutes
      end
      flash[:notice] = "Coaching dates successfully added!"
    else
      flash[:notice] = "Starting time cannot be before ending time!"
    end

    redirect_to :back
  end

  def remove_coaching_dates
    @coach = Coach.find(params[:id])
    starting = DateTime.strptime(params[:date] + " " + params[:starting_hour].to_s + ":" +params[:starting_minutes].to_s + " "+ params[:starting_am], "%m/%d/%Y %l:%M %p")
    ending = DateTime.strptime(params[:date] + " " + params[:ending_hour].to_s + ":" +params[:ending_minutes].to_s + " "+ params[:ending_am], "%m/%d/%Y %l:%M %p")
    if starting < ending
      @coach.coaching_dates.available.where("starting >= ? AND starting <= ?", starting, ending).destroy_all
      flash[:notice] = "Coaching dates successfully destroyed!"
    else
      flash[:notice] = "Starting time cannot be before ending time!"
    end

    redirect_to :back
  end

  def show_coaches
    @coaches = Coach.available
    @average_rating = {}
    @coaches.each do |coach|
      average = coach.reviews.map{|r| r.coach_grade}.inject{ |sum, el| sum + el }.to_f / coach.reviews.count
      @average_rating[coach] = coach.reviews.present? ? "#{average.round(2)}/5" : "N/A"
    end
  end

  def verify_phone
    @coach = Coach.find(params[:id])
  end

  def verify_token
    token = params[:token]
    coach = Coach.find(params[:id])
    if token == coach.phone_code
      coach.validated_phone = true
      coach.save
      flash[:notice] = "Phone successfully verified!"
      redirect_to edit_coach_path(coach)
    else
      flash[:notice] = "Incorrect code. Please try again!"
      redirect_to :back
    end
  end

  def resend_code
    @coach = Coach.find(params[:id])
    token = 4.times.map{rand(10)}.join
    @coach.phone_code = token
    @coach.save
    success = SendTwilioMessage.send_message(@coach.phone, token)
    if success
      flash[:notice] = "Code successfully resent!"
      redirect_to :back
    else
      flash[:notice] = "Invalid phone number. Please provide a valid one!"
      redirect_to edit_coach_path(@coach)
    end
  end

  private
  def coach_params
    params.require(:coach).permit(:first_name, :last_name, :about, :occupation, :phone)
  end
end
