class CoachingDatesController < ApplicationController
  def destroy
    authorize! :coaching_dates, @coach
    c = CoachingDate.find(params[:id])
    if c.destroy
      flash[:notice] = "Successfully destroyed coaching date!"
    else
      flash[:notice] = "Error while destroying coaching date. Please try again!"
    end
    redirect_to :back
  end

  def edit
    @coaching_date = CoachingDate.find(params[:id])
    @client = current_user.client
    @coach = @coaching_date.coach
    @public_key = ENV['STRIPE_PUBLISHABLE_KEY']
    @price = @coaching_date.coach.rate / 3
  end

  def update
    coaching_date = CoachingDate.find(params[:id])
    client = current_user.client
    price = coaching_date.coach.rate / 3
    if client.tokens >= price
      book_session(coaching_date,client)
      client.tokens -= price
      client.save
      BalanceLog.create(amount: price, client_id: client.id, coach_id: coaching_date.coach.id, from: "client")
      flash[:notice] = "Coaching session has been booked!"
      redirect_to show_coaches_coaches_path
    else
      flash[:notice] = "Insufficient funds!"
      redirect_to :back
    end
  end

  def pay_with_credit_card

    coaching_date = CoachingDate.find(params[:id])
    price = coaching_date.coach.rate / 3
    coach = coaching_date.coach
    client = current_user.client
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']

    token = params[:stripeToken]
    charge = Stripe::Charge.create(
      amount: price*100,
      currency: "usd",
      description: "Session with coach.user.email",
      source: token,
    )
    book_session(coaching_date, client)
    payment = Payment.create(coach_id: coach.id, client_id: client.id, coaching_date_id: coaching_date.id, charge_id: charge.id)
    flash[:notice] = "Payment successful! Coaching session has been booked!"
    redirect_to show_coaches_coaches_path
  end

private
  
  def book_session(coaching_date, client)
    coaching_date.client_id = client.id
    coaching_date.reserved = true
    coaching_date.paid = true
    coaching_date.save 
    UserMailer.send_reservation_email_coach(coaching_date.reload).deliver
    UserMailer.send_reservation_email_client(coaching_date.reload).deliver
  end

end
