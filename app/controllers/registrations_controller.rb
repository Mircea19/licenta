class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super
    @user.account_type = params[:user][:account_type]
    @user.save
  end

  def update
    super
  end


  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :account_type)
  end
end 