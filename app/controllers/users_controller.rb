class UsersController < ApplicationController
  before_filter :authenticate_user!, except: [:sign_in_from_admin]
  load_and_authorize_resource except: [:sign_in_from_admin]

  def edit
    @user = current_user
  end

  def update()
    prepare_params
    if @user.update_attributes(user_params)
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def sign_in_from_admin
    user = User.find(params[:id])
    if current_admin_user.present? && params[:token].present? && user.sign_in_token == params[:token]
      sign_in :user, user

      if user.account_type == "coach"
        if user.coach.blank?
          coach = Coach.new
          coach.user_id = user.id
          coach.save
        else
          coach = user.coach
        end
        redirect_to edit_coach_path(coach)
      elsif user.account_type == "client"
        if user.client.blank?
          client = Client.new
          client.user_id = user.id
          client.save
        else
          client = user.client
        end
        redirect_to edit_client_path(client)
      end
    else
      flash[:notice] = "Unauthorized access!"
      if current_user.present?
        sign_out_and_redirect(current_user)
      else
        redirect_to root_path
      end
    end
  end

  private
    def user_params
      params.require(:user).permit( :email, :password, :password_confirmation, :image, :remove_image)
    end

    def load_user
      user = User.find(params[:id])
    end

    def prepare_params
      if params[:user][:password].blank? && \
        params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
    end

  end

end
