class UserMailer < ActionMailer::Base
  default from: "coachingapp@rails.com", reply_to: "coachingapp@mail.com"
  layout "mail"

  def signup_confirmation(user)
    @user = user
    mail to: user.email, subject: "Sign Up Confirmation"
  end

  def reset_password(user)
    @user = user
    mail to: user.email, subject: "Reset Password"
  end

  def send_reservation_email_coach(coaching_date)
    load_data(coaching_date)
    mail to: @coach.user.email, subject: "Booked coaching session"
  end

  def send_reservation_email_client(coaching_date)
    load_data(coaching_date)
    mail to: @coach.user.email, subject: "Booked coaching session"
  end

private
  
  def load_data(coaching_date)
    @coaching_date = coaching_date
    @coach = @coaching_date.coach
    @client = @coaching_date.client
  end  


end
