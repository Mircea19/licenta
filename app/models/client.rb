class Client < ActiveRecord::Base
  belongs_to :user
  has_many :coaching_dates
  has_many :balance_logs
  has_many :payments
  has_many :reviews
  mount_uploader :photo, ImageUploader

  def name
    return self.first_name + " " + self.last_name
  end

  def name_with_email
    return self.name + " - " + self.user.email
  end
end
