class Coach < ActiveRecord::Base
  belongs_to :user
  has_many :coaching_dates
  has_many :payments
  has_many :reviews
  mount_uploader :photo, ImageUploader
  #validates_inclusion_of :rate, in: 10..200


  def name
    return (self.first_name.present? ? self.first_name : "")  + " " + (self.last_name.present? ? self.last_name : "")
  end

  def self.available
    Coach.all.select{|c| c.coaching_dates.upcoming.available.present?}
  end
end
