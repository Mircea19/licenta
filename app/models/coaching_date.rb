class CoachingDate < ActiveRecord::Base
  belongs_to :coach
  belongs_to :client
  has_one :payment
  has_one :review
  has_many :call_logs
  scope :available,  -> {where(client_id: nil)}
  scope :booked,  -> {where.not(client_id: nil)}
  scope :upcoming, -> {where('starting > ?', DateTime.now)}

  def init_call
    coach_number = self.coach.phone
    client_number = self.client.phone
    begin
      if coach_number.present? && client_number.present?
        call_log = CallLog.new
        call_log.coaching_date_id = self.id
        call_log.status = "intiated"
        call_log.ended_because = "application"
        call_log.save
        @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']

        from_number = ENV['TWILIO_NUMBER']

        call = @client.account.calls.create(url: self.twilio_url + "/api/twilio/confirm_coach?coaching_date_id=#{self.id}", # coach number
          method: "get",
          to: "#{coach_number}",  # regular user number
          from: from_number,
          statusCallback: self.twilio_url + "/api/twilio/call_ended_coach",
          "IfMachine" => "Continue")

        self.coach_call_sid = call.sid

        call = @client.account.calls.create(url: self.twilio_url + "/api/twilio/confirm_user?coaching_date_id=#{self.id}", # coach number
          method: "get",
          to: "#{client_number}",  # regular user number
          from: from_number,
          statusCallback: self.twilio_url + "/api/twilio/call_ended_user",
          "IfMachine" => "Continue")

        self.client_call_sid = call.sid
        self.save

        puts "Calling: " + call.to
        puts "Sid: " + call.sid
        return "Success"
      else
        puts "Missing phone numbers"
        if coach_profile.phone.present?
          return "Client number missing"
        else
          return "Coach number missing"
        end
      end
    rescue Exception => e
      puts e
      return false
    end
  end

  def recall_coach
    begin
      coach_number = self.coach.phone
      if coach_number.present?
        call_log = CallLog.new
        call_log.coaching_date_id = self.id
        call_log.status = "coach_recalled"
        call_log.ended_because = "application"
        call_log.save
        from_number = ENV['TWILIO_NUMBER']
        @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
        call = @client.account.calls.create(url: self.twilio_url + "/api/twilio/confirm_coach?coaching_date_id=#{self.id}", # coach number
          method: "get",
          to: "#{coach_number}",  # regular user number
          from: from_number,
          statusCallback: self.twilio_url + "/api/twilio/call_ended_coach",
          "IfMachine" => "Continue")

        self.coach_call_sid = call.sid
        self.save

        puts "Recalling coach: " + call.to
        puts "Sid: " + call.sid
        return "Success"
      else
        puts "Missing phone numbers"
        return "Client number missing"
      end
      return true
    rescue Exception => e
      puts e
      return false
    end
  end

  def recall_user
    begin
      client_number = self.client.phone
      if client_number.present?
        call_log = CallLog.new
        call_log.coaching_date_id = self.id
        call_log.status = "coach_recalled"
        call_log.ended_because = "application"
        call_log.save
        from_number = ENV['TWILIO_NUMBER']
        @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
        call = @client.account.calls.create(url: self.twilio_url + "/api/twilio/confirm_user?coaching_date_id=#{self.id}", # coach number
          method: "get",
          to: "#{client_number}",  # regular user number
          from: from_number,
          statusCallback: self.twilio_url + "/api/twilio/call_ended_user",
          "IfMachine" => "Continue")

        self.client_call_sid = call.sid
        self.save

        puts "Recalling client: " + call.to
        puts "Sid: " + call.sid
        return "Success"
      else
        puts "Missing phone numbers"
        return "Client number missing"
      end
      return true
    rescue Exception => e
      puts e
      return false
    end
  end

  def join_calls
    @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    @coach_call = @client.account.calls.get(self.coach_call_sid)
    @client_call = @client.account.calls.get(self.client_call_sid)
    call_log = CallLog.new
    call_log.coaching_date_id = self.id
    call_log.status = "calls_joined"
    call_log.ended_because = "application"
    call_log.save
    coach_number = @coach_call.to
    @coach_call.update(url: self.twilio_url + "/api/twilio/join_coach?coach_number=#{coach_number}", method: "GET")
    @client_call.update(url: self.twilio_url + "/api/twilio/join_user?coach_number=#{coach_number}", method: "GET")
  end

  def recall_user_after_joined
    begin
      @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
      @coach_call = @client.account.calls.get(self.coach_call_sid)
      regular_number = self.client.phone
      if regular_number.present?
        call_log = CallLog.new
        call_log.coaching_date_id = self.id
        call_log.status = "user_recalled_after_joined"
        call_log.ended_because = "application"
        call_log.save
        from_number = ENV['TWILIO_NUMBER']
        @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']

        call = @client.account.calls.create(url: self.twilio_url + "/api/twilio/rejoin_user", # coach number
          method: "get",
          to: "#{regular_number}",  # regular user number
          from: from_number,
          statusCallback: self.twilio_url + "/api/twilio/call_ended_user",
          "IfMachine" => "Continue")      
        self.client_call_sid = call.sid
        self.save
        @coach_call.update(url: self.twilio_url + "/api/twilio/hold_coach_after_joined", method: "GET")

        puts "Recalling user after joined: " + call.to
        puts "Sid: " + call.sid
        return "Success"
      else
        puts "Missing phone numbers"
        return "Client number missing"
      end
      return true
    rescue Exception => e
      puts e
      return false
    end
  end

  def recall_coach_after_joined
    begin
      @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
      @client_call = @client.account.calls.get(self.client_call_sid)
      coach_number = self.coach.phone
      if coach_number.present?
        call_log = CallLog.new
        call_log.coaching_date_id = self.id
        call_log.status = "coach_recalled_after_joined"
        call_log.ended_because = "application"
        call_log.save
        from_number = ENV['TWILIO_NUMBER']
        @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
        call = @client.account.calls.create(url: self.twilio_url + "/api/twilio/rejoin_coach", # coach number
          method: "get",
          to: "#{coach_number}",  # regular user number
          from: from_number,
          statusCallback: self.twilio_url + "/api/twilio/call_ended_coach",
          "IfMachine" => "Continue")
        self.coach_call_sid = call.sid
        self.save
        @client_call.update(url: self.twilio_url + "/api/twilio/hold_user_after_joined", method: "GET")

        puts "Recalling coach after joined: " + call.to
        puts "Sid: " + call.sid
        return "Success"
      else
        puts "Missing phone numbers"
        return "Client number missing"
      end
      return true
    rescue Exception => e
      puts e
      return false
    end
  end

  def end_user
    begin
      @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']

      @client_call = @client.account.calls.get(self.client_call_sid)
      @client_call.update(url: self.twilio_url + "/api/twilio/end_user",
          method: "GET")

      return true
    rescue Exception => e
      puts e
      return false
    end
  end

  def end_coach
    begin
      @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']

      @coach_call = @client.account.calls.get(self.coach_call_sid)
      @coach_call.update(url: self.twilio_url + "/api/twilio/end_coach",
          method: "GET")

      return true
    rescue Exception => e
      puts e
      return false
    end
  end

  def end_call
    begin
      @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
      @client_call = @client.account.calls.get(self.client_call_sid)
      @coach_call = @client.account.calls.get(self.coach_call_sid)
      call_log = CallLog.new
      call_log.coaching_date_id = self.id
      call_log.status = "review_entered"
      call_log.ended_because = "application"
      call_log.save
      @coach_call.update(url: self.twilio_url + "/api/twilio/finished_session_coach",
            method: "GET")
      @client_call.update(url: self.twilio_url + "/api/twilio/submit_review",
            method: "GET")

      return true 
    rescue Exception => e
      puts e
      return false
    end
  end

  def forward(min)
    self.starting -= min.minutes
    self.call_initiated -= min.minutes
    self.call_logs.each do |cl|
      cl.created_at -= min.minutes
      cl.save
    end
    self.save
  end

  def twilio_url
    return "http://2b31832b.ngrok.io"
  end

end
