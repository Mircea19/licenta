class Payment < ActiveRecord::Base
  belongs_to :client
  belongs_to :coach
  belongs_to :coaching_date
end
