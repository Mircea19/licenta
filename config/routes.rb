Rails.application.routes.draw do

  # the page root
  root 'users#edit'

  # routes for Devise and Omniauth
  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks", registrations: "registrations" }

  # routes for Active Admin
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # routes for the API's
  namespace :api, defaults: {format: :json} do
    mount TolSkitSessions::Engine => "/"
    mount TolSkitSessionsFacebook::Engine => "sessions/facebook"
    mount TolSkitSessionsTwitter::Engine => "sessions/twitter"
    mount TolSkitSessionsInstagram::Engine => "sessions/instagram"
    
    resources :users
    resources :admin_sessions, only: [:create]
    resources :admin_users, only: [:show]
    resources :twilio do
      collection do 
        get :confirm_coach
        get :confirm_user
        post :call_ended_coach
        post :call_ended_user
        get :end_user
        get :end_coach
        get :hold_coach
        get :hold_user
        get :after_confirmation_user
        get :after_confirmation_coach
        get :join_coach
        get :join_user
        get :rejoin_coach
        get :rejoin_user
        get :hold_user_after_joined
        get :hold_coach_after_joined
        get :finished_session_coach
        get :submit_review
        get :after_first_review_enter
        get :invalid_first_grade
        get :submit_second_grade
        get :after_second_review_enter
        get :invalid_second_grade
      end
    end
  end

  # routes for locale change
  get 'sessions/:locale', to: "sessions#switch", as: :sessions

  # routes for Users
  resources :users

  resources :coaching_dates do
    member do
      post :pay_with_credit_card
    end
  end

  resources :coaches do 
    member do
      get :set_coaching_dates
      get :financial_details
      get :verify_phone
      post :set_financial_details
      post :add_coaching_dates
      post :remove_coaching_dates
      post :verify_token
      post :resend_code
    end
    collection do
      get :show_coaches
    end
  end

  resources :clients do
    member do
      get :verify_phone
      post :verify_token
      post :resend_code
    end
  end

  # route for User Sign Out
  devise_scope :user do
    get 'sign_out', to: 'devise/sessions#destroy', as: :signout
    post 'sign_in_from_admin' => 'users#sign_in_from_admin'
  end

  # routes for Omniauth
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')

end
