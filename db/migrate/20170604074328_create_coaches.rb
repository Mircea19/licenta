class CreateCoaches < ActiveRecord::Migration
  def change
    create_table :coaches do |t|
      t.string :user_id
      t.text :about
      t.string :first_name
      t.string :last_name
      t.string :occupation
      t.string :photo
      t.integer :tokens

      t.timestamps null: false
    end
  end
end
