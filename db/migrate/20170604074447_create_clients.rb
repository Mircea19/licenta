class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.integer :user_id
      t.text :about
      t.string :first_name
      t.string :last_name
      t.string :occupation
      t.integer :tokens
      t.string :photo

      t.timestamps null: false
    end
  end
end
