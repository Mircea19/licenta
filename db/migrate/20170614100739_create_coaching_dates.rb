class CreateCoachingDates < ActiveRecord::Migration
  def change
    create_table :coaching_dates do |t|
      t.integer :coach_id
      t.integer :client_id
      t.boolean :reserved
      t.boolean :paid
      t.datetime :starting
      t.integer :duration
      t.integer :paid_duration

      t.timestamps null: false
    end
  end
end
