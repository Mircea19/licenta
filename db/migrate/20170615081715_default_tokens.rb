class DefaultTokens < ActiveRecord::Migration
  def change
    change_column :coaches, :tokens, :integer, default: 0
    change_column :clients, :tokens, :integer, default: 0
  end
end
