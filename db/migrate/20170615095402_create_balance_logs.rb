class CreateBalanceLogs < ActiveRecord::Migration
  def change
    create_table :balance_logs do |t|
      t.integer :amount
      t.integer :client_id
      t.integer :coach_id
      t.string :from

      t.timestamps null: false
    end
  end
end
