class AddRateToCoaches < ActiveRecord::Migration
  def change
    add_column :coaches, :rate, :integer
  end
end
