class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :coaching_date_id
      t.integer :amount
      t.integer :client_id
      t.integer :coach_id
      t.string :charge_id

      t.timestamps null: false
    end
  end
end
