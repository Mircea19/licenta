class AddValidatedPhoneToClients < ActiveRecord::Migration
  def change
    add_column :clients, :validated_phone, :boolean, default: false
  end
end
