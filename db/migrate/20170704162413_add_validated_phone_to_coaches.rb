class AddValidatedPhoneToCoaches < ActiveRecord::Migration
  def change
    add_column :coaches, :validated_phone, :boolean, default: false
  end
end
