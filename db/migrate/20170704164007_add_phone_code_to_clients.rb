class AddPhoneCodeToClients < ActiveRecord::Migration
  def change
    add_column :clients, :phone_code, :string
  end
end
