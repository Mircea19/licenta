class AddPhoneCodeToCoaches < ActiveRecord::Migration
  def change
    add_column :coaches, :phone_code, :string
  end
end
