class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :coach_id
      t.integer :client_id
      t.integer :coaching_date_id
      t.integer :session_grade
      t.integer :coach_grade

      t.timestamps null: false
    end
  end
end
