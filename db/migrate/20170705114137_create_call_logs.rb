class CreateCallLogs < ActiveRecord::Migration
  def change
    create_table :call_logs do |t|
      t.integer :coaching_date_id
      t.string :status
      t.string :ended_because

      t.timestamps null: false
    end
  end
end
