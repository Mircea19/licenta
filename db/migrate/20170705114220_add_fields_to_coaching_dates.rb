class AddFieldsToCoachingDates < ActiveRecord::Migration
  def change
    add_column :coaching_dates, :coach_call_sid, :string
    add_column :coaching_dates, :client_call_sid, :string
    add_column :coaching_dates, :call_initiated, :datetime
  end
end
