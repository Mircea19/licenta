class ChangeDefault < ActiveRecord::Migration
  def change
    change_column :coaching_dates, :recalled_coach, :integer, default: 0
  end
end
