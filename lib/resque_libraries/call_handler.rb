module ResqueLibraries
class CallHandler
  @queue = :general
  
  def self.perform
    while true
      puts "#{Time.now} Resque library call handler call_handler.rb start"
      CoachingDate.booked.where('(starting < ?) AND (starting > ?)', DateTime.now + 5.seconds, DateTime.now - 30.minutes).each do |cd|
        if cd.client_call_sid.blank?
          status = cd.init_call 
          if status != "Success"
            call_log = CallLog.new
            call_log.coaching_date_id = cd.id
            call_log.ended_because = status
            call_log.status = "failed"
            call_log.save
            cd.client_call_sid = "failed"
            cd.save
          else
            cd.call_initiated = DateTime.now.to_datetime
            cd.save
          end
        elsif cd.client_call_sid.present? && cd.client_call_sid != "failed"
          call_log = CallLog.new
          call_log.coaching_date_id = cd.id
          last_call_log = cd.call_logs.sort_by(&:created_at).try(:last)
          all_call_logs = cd.call_logs
          if all_call_logs.present?
            call_logs_messages = all_call_logs.collect(&:status)
          else
            call_logs_messages = [""]
          end
          @client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
          @client_call = @client.account.calls.get(cd.client_call_sid)
          @coach_call = @client.account.calls.get(cd.coach_call_sid)

          if ((DateTime.now.to_datetime - cd.call_initiated.to_datetime) * 24 * 60 * 60).to_f / 60 <= 1 
            if @coach_call.status == "in-progress" && @client_call.status != "in-progress" && @client_call.status != "ringing" && cd.recalled_user < 3
              cd.recalled_user += 1
              cd.save
              cd.recall_user
            elsif @coach_call.status != "in-progress" && @coach_call.status != "ringing" && @client_call.status == "in-progress" && cd.recalled_coach < 3
              cd.recalled_coach += 1
              cd.save
              cd.recall_coach
            end
          elsif ((DateTime.now.to_datetime - cd.call_initiated.to_datetime) * 24 * 60 * 60).to_f / 60 <= 22 
            #user_call_log = cd.call_logs.where("status = ? AND ended_because = ?","hung_up", "user").last
            #coach_call_log = cd.call_logs.where("status = ? AND ended_because = ?","hung_up", "coach").last
            calls_joined = cd.call_logs.where("status = ?","calls_joined").limit(1).present?
            if (@coach_call.status == "in-progress" && @client_call.status != "in-progress" && @client_call.status != "ringing") && calls_joined && cd.recalled_user <3
              cd.recalled_user += 1
              cd.save
              cd.recall_user_after_joined
            elsif  (@coach_call.status != "in-progress" && @coach_call.status != "ringing" && @client_call.status == "in-progress")  && calls_joined && cd.recalled_coach <3
              cd.recalled_coach += 1
              cd.save
              cd.recall_coach_after_joined
            elsif (@coach_call.status == "in-progress" && @client_call.status != "in-progress" && @client_call.status != "ringing") 
              cd.end_coach
            elsif (@coach_call.status != "in-progress" && @coach_call != "ringing" && @client_call.status == "in-progress")
              cd.end_user
            elsif @coach_call.status == "in-progress" && 
              @client_call.status == "in-progress" && 
              (last_call_log.present? && last_call_log.status != "calls_joined")
              cd.recalled_user = 0
              cd.recalled_coach = 0
              cd.save
              cd.join_calls
            end
          elsif call_logs_messages.exclude?("review_entered")
            cd.end_call
          end
        end
      end
      puts "#{Time.now} Resque library call handler call_handler.rb end"
      sleep(10)
    end
  end
end
end