class SendTwilioMessage
  def self.send_message(to, token)
    account_sid = ENV['TWILIO_ACCOUNT_SID']
    auth_token = ENV['TWILIO_AUTH_TOKEN']
    client = Twilio::REST::Client.new account_sid, auth_token

    from = ENV['TWILIO_NUMBER']
    begin 
      client.account.messages.create(
        from: from,
        to: to,
        body: "Welcome to Coaching App. Your confirmation code is: #{token}"
      )
      return true
    rescue 
      return false
    end
  end

end